package crawl

import (
	"time"

	"gitlab.com/btlike/storage/parser"
	"gitlab.com/btlike/storage/utils"
)

//Run the spider
func Run() {
	Manager.run()
	Crawl()
}

type job struct {
	ip string
	ih string
}

//Crawl from xunlei ...
func Crawl() {
	worker := func(jobs <-chan job, resultChan chan<- string) {
		crawl := newCrawl()
		for infohash := range jobs {
			if !Manager.crawStatus[Xunlei].pauseCrawl {
				//至少有一个引擎在服务时，直接删除即可，防止引擎都不服务时，疯狂删数据
				resultChan <- infohash.ih
			}

			var data parser.MetaInfo
			var err error

			if !Manager.crawStatus[Xunlei].pauseCrawl && !Manager.crawStatus[Xunlei].stopCrawl {
				data, err = parser.DownloadVuze(infohash.ih, crawl.xunleiClient)
				if err != nil {
					utils.Log().Println(err)
					if err == parser.ErrNotFound {
						Manager.crawStatus[Xunlei].notFoundCount++
					} else {
						Manager.crawStatus[Xunlei].refuseCount++
					}
					continue
				} else {
					//没报错，进入存储流程
					goto store
				}
			}

		store:
			err = addToES(&data, infohash.ip)
			if err != nil {
				utils.Log().Println(err)
			}
			resultChan <- infohash.ih
			Manager.storeCount++
		}
	}

	jobChan := make(chan job, DownloadChanLength)
	resultChan := make(chan string, DownloadChanLength)
	defer close(resultChan)
	for i := 0; i < 10; i++ {
		go worker(jobChan, resultChan)
	}

	go func() {
		var infohashs []string
		for infohash := range resultChan {
			if len(infohash) == 40 {
				infohashs = append(infohashs, infohash)
				if len(infohashs) >= 30 {
					//del from redis
					utils.Config.Redis.HDel("infohash", infohashs...)
					infohashs = make([]string, 0)
				}
			}
		}
	}()

	var cursor uint64 = 0
	cursor, err := utils.Config.Redis.Get("infohash_set_cs").Uint64()
	if err != nil {
		utils.Log().Println(err)
		return
	}

	for {
		if Manager.crawStatus[Xunlei].pauseCrawl || Manager.crawStatus[Xunlei].stopCrawl {
			utils.Log().Println("全部引擎拒绝服务,暂停抓取,等待10分钟")
			// time.Sleep(time.Minute * 10)
			Manager.crawStatus[Xunlei] = &crawStatus{}
			continue
		}

		ihs, cs, err := utils.Config.Redis.SScan("infohash_set", cursor, "", 100).Result()
		if err != nil {
			utils.Log().Println("scan err", err)
			time.Sleep(time.Second * 60)
			continue
		}
		utils.Config.Redis.Set("infohash_set_cs", cs, 0)
		cursor = cs

		utils.Log().Println("get infohash from redis", len(ihs))
		if len(ihs) == 0 {
			time.Sleep(time.Second * 60)
		}
		for i := 0; i < len(ihs); i++ {
			if len(ihs[i]) == 40 {
				jobChan <- job{ih: ihs[i]}
			}
		}
	}
}
